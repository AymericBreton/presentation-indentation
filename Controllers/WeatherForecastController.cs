using Microsoft.AspNetCore.Mvc;

namespace Indentation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly bool _isWeatherForecast;
        private readonly int _year;

        public WeatherForecastController()
        {
            _isWeatherForecast = true;
            _year = 2023;
        }

        [HttpGet(Name = "GetWeatherForecast/{id}")]
        public IActionResult Get(int? id)
        {
            try
            {
                if (id == null)
                {
                    throw new ArgumentNullException(nameof(id));
                }
                else
                {
                    if (_isWeatherForecast)
                    {
                        List<WeatherForecast> weatherForecasts;
                        if (_year < DateTime.Now.Year)
                        {
                            weatherForecasts = Enumerable.Range(1, 10).Select(index => new WeatherForecast
                            {
                                Date = DateTime.Now.AddDays(index),
                                TemperatureC = Random.Shared.Next(-20, 55),
                                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
                            }).ToList();
                        }
                        else
                        {
                            weatherForecasts = Enumerable.Range(1, 5).Select(index => new WeatherForecast
                            {
                                Date = DateTime.Now.AddDays(index),
                                TemperatureC = Random.Shared.Next(-10, 35),
                                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
                            }).ToList();
                        }
                        return Ok(weatherForecasts);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }
    }
}